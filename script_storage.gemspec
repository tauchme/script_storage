# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'script_storage'
  s.version     = '2.0.0'
  s.summary     = 'Save simple data in your script'
  s.description = 'A simple gem that lets you store basic data in your ruby script without needing to create a separate file for it'
  s.authors     = ['TauchMe']
  s.metadata    = {
    'homepage_uri' => 'https://gitlab.com/tauchme/script_storage',
    'documentation_uri' => 'https://gitlab.com/tauchme/script_storage',
    'source_code_uri' => 'https://gitlab.com/tauchme/script_storage'
  }
  s.email       = 'tauchme@mocchimochi.dev'
  s.files       = ['lib/script_storage.rb', 'lib/script_storage_item.rb']
  s.homepage    =
    'https://rubygems.org/gems/script_storage'
  s.license = 'MIT'
end
