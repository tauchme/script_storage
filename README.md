[![Gem Version](https://badge.fury.io/rb/script_storage.svg)](https://badge.fury.io/rb/script_storage)
# script_storage

You have a Ruby script, and you also have a great database server installed. But you don't want to hunt a bunny with a cannon.

We all remember creating our first "database" in a text file, and it worked, right? So why not implement that logic directly into the script, keeping it as a single Ruby file?

## How to install

Install the gem systemwide with the command below or use [inline bundler](https://bundler.io/guides/bundler_in_a_single_file_ruby_script.html) in your script.

```bash
gem install 'script_storage'
```

## How it works

This gem uses ruby’s `__END__` syntax to store persistent data after it. No more, no less.

### How it really works

When you load the gem, all your saved data (under the `__END__`) is read and stored in a class variable `@@data`, so it can be accessed by all parts of your script. Now each time you perform a `CRUD` operation, it will look like it is reading from your script, but indeed, it is taken from `@@data`. This means your script isn't actually being read repeatedly, but it looks like it is, which is beneficial because your script is loaded into memory just once, as it is supposed to be.

## How to use it

To store data as `String`, just write:

```ruby
ScriptStorage.write('color', 'yellow')
```

This is how it looks with an `Array`:

```ruby
ScriptStorage.write('color', ['yellow', 'red'])
```

To retrieve the data, just write:

```ruby
ScriptStorage.read('color')
```

To delete the data, just write:

```ruby
ScriptStorage.delete('color')
```

To delete all the data and also the `__END__`, just write:

```ruby
ScriptStorage.delete_all
```

And that’s all! You can store values of type: `String`, `Integer` and `Array`.

This is how stored data looks like:

```ruby
require 'script_storage'

# your stuff

__END__
color: yellow
step(integer): 1
swords(array): katana, nodachi
```

---

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/K3K281ONV)
