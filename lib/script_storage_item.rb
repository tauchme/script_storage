# frozen_string_literal: true

class Item
  attr_accessor :key, :value

  def initialize(key, value)
    @key = key
    @value = value.is_a?(Array) ? value.dup : value
  end

  def show
    type = value.class.to_s.downcase
    formatted_value = value.is_a?(Array) ? value.join(', ') : value
    "#{key}(#{type}): #{formatted_value}"
  end

  def ==(other)
    key == (other.is_a?(String) ? other : other.key)
  end
end
