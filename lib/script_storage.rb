# frozen_string_literal: true

if $PROGRAM_NAME == 'irb'
  puts <<~HEREDOC
    <>SCRIPT_STORAGE NOT LOADED<>
    It seems that you are using irb, please DON'T use this gem as it is supposed to be used in a script file.
  HEREDOC
  return
end

require 'script_storage_item'

class ScriptStorage
  @@data = []

  class << self
    def path
      "#{Dir.pwd}/#{$PROGRAM_NAME}"
    end

    def read(key)
      item = @@data.find { |item| item.key == key }
      item&.value
    end

    def write(key, value)
      item = @@data.find { |item| item.key == key }
      if item
        item.value = value
      else
        @@data << Item.new(key, value)
      end
      write_file
    end

    def delete(key)
      @@data.reject! { |item| item.key == key }
      write_file
    end

    def delete_all
      file_data = File.read(path).split("\n__END__\n")[0]
      File.write(path, file_data)
    end

    private

    def write_file
      file_data = "#{File.read(path).split("\n__END__\n")[0]}\n__END__\n"
      @@data.each { |v| file_data += "#{v.show}\n" }
      File.write(path, file_data)
    end

    def load_storage
      file_content = File.read(path)
      return if file_content.split("\n__END__\n").length == 1

      file_data = file_content.split("\n__END__\n").last
      file_data.scan(/(\w+)(\((\w+)\))?:(.*)/).each do |raw|
        key, _full_type, type, raw_value = raw
        value = case type
                when 'array'
                  raw_value.split(',').map(&:strip)
                when 'integer'
                  raw_value.to_i
                else
                  raw_value.strip
                end
        @@data << Item.new(key.strip, value)
      end
    end
  end

  load_storage
end