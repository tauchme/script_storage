# frozen_string_literal: true

require 'script_storage'

puts "Last time I liked the #{ScriptStorage.read('color')} color."
puts 'I will just forget about that color... (Check how the code changed)'
ScriptStorage.delete('color')
puts 'But now I will go with...'
value = gets
ScriptStorage.write('color', value)
puts '(Check the code again)'

__END__
color(string): Yellow
