# frozen_string_literal: true

require 'script_storage'

def setup
  puts 'I am doing some setup...'
end

def main
  puts 'The setup was already done. Some other thigs will start now...'
end

case ScriptStorage.read('step')
when 1
  setup
  ScriptStorage.write('step', 2)
  main
when 2
  main
end

__END__
step(integer): 1
